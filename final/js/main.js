$(function () {
    var roundCount;
    var gameOver = false;
    var go = false;
    var sequencePlaying;
    var highest, $highest;
    var boxSelect = ['g', 'b', 'r', 'y', 'o', 'p'];
    var sequence = [];
    var inputSeq = [];
    var round, $round;
    var highest;
    var $playBtn;
    var $g;
    var $b;
    var $r;
    var $y;
    var $o;
    var $p;
    
    $g = $('#g');
    $b = $('#b');
    $r = $('#r');
    $y = $('#y');
    $o = $('#o');
    $p = $('#p');
    $round = $('#round')
    $highest = $('#highest');
    $playBtn = $('#playBtn');
    
    if(!localStorage.getItem('highest')) {
        localStorage.setItem('highest', 0);
    }   
    
    function loadHighScore () {
        highest = Number(localStorage.getItem('highest'));
        $highest.text("High Score: " + highest);
    }
    
    function gLight() {
        $g.css("background-color", "lightgreen");
    }
                    
    function bLight() {
        $b.css("background-color", "lightblue");
    }
                        
    function oLight() {
        $o.css("background-color", "#ffd27f");
    } 

    function yLight() {
        $y.css("background-color", "#ffff7f");
    }  
                                                
    function rLight() {
        $r.css("background-color", "lightcoral");
    } 
                    
    function pLight() {
        $p.css("background-color", "#ffdfe5");
    }    
    
    function revertColor() {
        $g.css("background-color", "darkgreen")
        $b.css("background-color", "darkblue")
        $o.css("background-color", "#b27300")
        $y.css("background-color", "#999900")
        $r.css("background-color", "darkred")
        $p.css("background-color", "#b2868e")
    }
    
    g.addEventListener('click', (event) => {
        if(go) {
            inputSeq.push('g');
            checkInput();
            gLight();
            setTimeout(() => {
                revertColor();
            }, 300);
        }
    })
    b.addEventListener('click', (event) => {
        if(go) {
            inputSeq.push('b');
            checkInput();
            bLight();
            setTimeout(() => {
                revertColor();
            }, 300);
        }
    })    
    o.addEventListener('click', (event) => {
        if(go) {
            inputSeq.push('o');
            checkInput();
            oLight();
            setTimeout(() => {
                revertColor();
            }, 300);
        }
    })    
    y.addEventListener('click', (event) => {
        if(go) {
            inputSeq.push('y');
            checkInput();
            yLight();
            setTimeout(() => {
                revertColor();
            }, 300);
        }
    })    
    r.addEventListener('click', (event) => {
        if(go) {
            inputSeq.push('r');
            checkInput();
            rLight();
            setTimeout(() => {
                revertColor();
            }, 300);
        }
    })    
    p.addEventListener('click', (event) => {
        if(go) {
            inputSeq.push('p');
            checkInput();
            pLight();
            setTimeout(() => {
                revertColor();
            }, 300);
        }
    })
    
    function checkInput() {
        
        
        if (inputSeq[inputSeq.length - 1] !== sequence[inputSeq.length - 1]) {
            go = false; 
            sequencePlaying = false;
            sequence = [];
            document.getElementById("round").textContent=("oof");
            setTimeout(() => {
            document.getElementById("round").textContent=("Game Over");
            }, 1000);
        }
        
        if(round === inputSeq.length && go === true) {
            inputSeq = [];
            sequencePlaying = true;
            roundCount = 0;
            document.getElementById("round").textContent=("Round: " + round);
            
            if (round > highest) {
                highest = round;
                localStorage.setItem('highest', highest);
                document.getElementById("highest").textContent=("High Score: " + highest);
            }
            round++
            intervalID = setInterval(playSequence, 700); 
        }
    }
    
    
    function addToSeq() {
        let num = boxSelect.length;
        nextColor = boxSelect[Math.floor(Math.random() * num)];
        sequence.push(nextColor);
    }
    
   
    
    function playSequence() {
        addToSeq();
        go = false;
        
        if (roundCount === round) {   //!~!!!!!!!!!!!!!!!!!!!!!!
            clearInterval(intervalID);
            sequencePlaying = false;
            revertColor();
            go = true;
        }
        
        if (sequencePlaying) {
            revertColor();
            setTimeout(() => {
                if (sequence[roundCount] === 'g') gLight();
                if (sequence[roundCount] === 'b') bLight();
                if (sequence[roundCount] === 'o') oLight();
                if (sequence[roundCount] === 'y') yLight();
                if (sequence[roundCount] === 'r') rLight();
                if (sequence[roundCount] === 'p') pLight();
                roundCount++;
            }, 200);
        }  
    } 
    function playGame() {
        document.getElementById("round").textContent=("Round: " + 0);
        loadHighScore();
        sequence = [];
        inputSeq = [];
        intervalID = 0;
        roundCount = 0;
        round = 1;
        $round = 1;
        
        sequencePlaying = true;
        
        intervalID = setInterval(playSequence, 700)
    }

    loadHighScore();
    
    playBtn.addEventListener('click', (event) => {
        playGame();
    });
});